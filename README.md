# ns-3 InF channel modeling

This project operates with the implementation of the indoor factory channel model as an extension tool for one of the most well-known open-source software simulators, ns-3.

## Getting started

This new implementation works with the NR module of the ns-3 simulator. Therefore, the ns-3 simulator must be installed and the NR module permissions must be obtained.


## Add the files
Inside this repository, you will find the files that need to be replaced to add the Indoor Factory scenario to NS3 simulator. The following instructions should be applied:

Add files using the command line:

```
cd existing_repo
git remote add origin https://gitlab.com/andre.ramosp/ns-3-inf-channel-modeling.git
git branch -M main
git push -uf origin main
```

## Step 1

The following files must be replaced in the src/propagation/model folder of the ns-3 simulator:

> channel-condition-model.cc

> channel-condition-model.h

> three-gpp-propagation-loss-model.cc

> three-gpp-propagation-loss-model.h

## Step 2
The following files must be replaced in the src/spectrum/model folder of the ns-3 simulator:

> three-gpp-channel-model.cc

## Step 3

The following files must be replaced in the contrib/nr/helper folder of the ns-3 simulator:

> cc-bwp-helper.cc

> cc-bwp-helper.h

> nr-helper.c

## Step 4 Test the InF channel model installation

Let's run the following commands for testing:

```
$ ./waf configure
```

If previous command returns successfully, next command must be run:

```
$ ./waf
```

## Step 5 
Finally, the example _"three-gpp-industrial-channel-example.cc"_ must be added and run in the **scratch** folder of the ns-3 simulator. This runnable script was used to obtain the paper's results ("Implementation and Calibration of the 3GPP Industrial Channel Model for ns-3"), which collects data during the simulation run. The following output files are obtained:

- SINR values for all the 180 UEs
- RSSI values for all the 180 UEs
- Coupling Loss Values for all the 180 UEs, based on pathloss
- UE positions
- gNB positions
- Distances of UEs from the gNBs to which they are attached

The file names by default start with the prefixes such as "sinrs", "rssi", "gnb-positions,", "ue-positions", and "coupling" which are followed by the  string that briefly describes the configuration parameters that are being set in the specific simulation execution.

Regarding Geometry without noise results, they were later calculated from the coupling loss results, taking into account the gain of received signal of the BS server (who has the best coupling loss) and the non-BS servers (the rest of BS).
 
 ## Paper

Implementation and Calibration of the 3GPP Industrial Channel Model for ns-3. For The Workshop on ns-3 (WNS3) 2022. Virtual event. [Accepted].


***
## Abstract
The Fifth Generation (5G) of mobile communications has brought a change of paradigm in the way cellular technologies are conceived. 5G has been designed to provide services not only to people, but also to industries and verticals. One of the verticals that will benefit most from the 5G is the Industry 4.0. Channel modeling for this vertical is receiving significant attention, mainly due to the increased complexity that comes with multipath fading scenarios. In order to overcome this problem, the Third Generation Partnership Project (3GPP) defined a new stochastic channel model as part of Release-16, called Indoor Factory (InF). This paper describes the implementation process of this channel model as an extension tool for one of the most well-known open-source software simulators, ns-3. Calibration results have been obtained and compared with other 3GPP references. This work permits to use ns-3 as a reliable tool for evaluating new industrial scenarios and use cases.


## Acknowledgment
Part of this work has been performed in the framework of the H2020 projects 5G-SMART and iNGENIOUS, co-funded by the EU. The views expressed in this paper are those of the authors and do not necessarily represent the views of these projects. The work has also been supported by the Spanish Ministry of Science, Innovation and University under the project RTI2018-099880-B-C31. Saul Inca and Jose F. Monserrat would like to acknowledge the contributions of their colleagues from 5G-SMART.

## Authors

* Andrea Ramos 
* Yanet Estrada
* Miguel Cantero
* Jaime Romero
* David Martín-Sacristán
* Saúl Inca
* Manuel Fuentes
* Jose F. Monserrat 

From iTEAM Research Institute, Universitat Politècnica de València, Spain and Communications for Future Industry Verticals S.L. (Fivecomm), Spain.

## Support
For more information or help, please feel free to contact the following email address: 

> anrapil@iteam.upv.es

## License
This implementation is an open source projects.



